CC = gcc
CFLAGS = -Wall -m32

all: main.o x86_function.o
	$(CC) $(CFLAGS) -o example main.o x86_function.o

x86_function.o: x86_function.asm
	nasm -o x86_function.o -f elf -l x86_function.lst x86_function.asm

main.o: main.c
	$(CC) $(CFLAGS) -c -o main.o main.c

clean:
	rm -f *.o
